<?php



Route::get('/basic_response', function () {
    return 'Hello World';
});


Route::get('/header',function() {
    return response("Hello", 200)->header('Content-Type', 'text/html');
});



Route::get('json',function() {
    return response()->json(['name' => 'Virat Gandhi', 'state' => 'Gujarat']);
});



Route::get('/cookie',function() {
    return response("Hello", 200)->header('Content-Type', 'text/html')
        ->withcookie('name_test','Virat Gandhi');
});


Route::get('/cookie/set','CookieController@setCookie');
Route::get('/cookie/get','CookieController@getCookie');



Route::get('/test', function() {
    return view('test');
});


Route::get('/test1', function() {
    return view('test');
});








Route::get('/test/abc/df/ef', ['as'=>'testing',function() {
    return view('child',['as'=>'testing','param' => 'rvm']);
}]);



Route::get('/test_redirect',function() {
    return redirect()->route('testingu');
});


/*
 *
 https://www.tutorialspoint.com/laravel/laravel_localization.htm
 *
 */

Route::get('localization/{locale}','LocalizationController@index');